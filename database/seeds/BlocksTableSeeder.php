<?php

use Illuminate\Database\Seeder;

use App\Block;

class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$block1 = new Block;
    	$block1->icon = 'flash_on';
    	$block1->title = 'Titulo 1';
    	$block1->description = 'Descripcion 1';
    	$block1->color= 'yellow';
        $block1->save();

    	$block2 = new Block;
    	$block2->icon = 'people';
    	$block2->title = 'Titulo 2';
    	$block2->description = 'Descripcion 2';
    	$block2->color= 'yellow';
        $block2->save();

    	$block3 = new Block;
    	$block3->icon = 'settings';
    	$block3->title = 'Titulo 3';
    	$block3->description = 'Descripcion 3';
    	$block3->color= 'yellow';
        $block3->save();

    }
}
