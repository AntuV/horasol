<?php

use Illuminate\Database\Seeder;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colorsEng = ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange', 'deep-orange', 'brown', 'grey', 'blue-grey', 'black', 'white'];

        $colorsSpa = ['Rojo', 'Rosa', 'Púrpura', 'Púrpura intenso', 'Añil', 'Azul', 'Celeste', 'Cyan', 'Azul verdoso', 'Verde', 'Verde claro', 'Lima', 'Amarillo', 'Ámbar', 'Naranja', 'Naranja intenso', 'Marrón', 'Gris', 'Azul grisoso', 'Negro', 'Blanco'];

        for($i=0; $i<count($colorsEng); $i++){
        	$color = new \App\Color;
        	$color->name_english = $colorsEng[$i];
            $color->name_spanish = $colorsSpa[$i];
        	$color->save();
        }
    }
}
