<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BlocksTableSeeder::class);
        $this->call(CoverTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(MessageTableSeeder::class);
        $this->call(ColorsSeeder::class);
    }
}
