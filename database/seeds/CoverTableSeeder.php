<?php

use Illuminate\Database\Seeder;

use App\Cover;

class CoverTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cover = new Cover;
    	$cover->url = 'assets/covers/1513648984.jpg';
    	$cover->title = 'Horasol';
    	$cover->description = 'Venta de regalos personalizados y servicio de planificación de eventos';
    	$cover->save();
    }
}
