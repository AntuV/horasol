<?php

use Illuminate\Database\Seeder;
use App\Message;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message1 = new Message;
    	$message1->email = 'a@correo.com';
    	$message1->name = "Antú";
    	$message1->surname = "Villegas";
    	$message1->subject = "Consulta babyshower";
    	$message1->body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas magna arcu, scelerisque ut ex iaculis, luctus rhoncus velit. Etiam gravida dictum elit, vitae fringilla velit dictum et. Mauris consequat egestas pretium. Mauris non mi sit amet ligula venenatis rhoncus nec vitae purus. Nullam lobortis, odio at elementum mollis, mi elit auctor augue, vitae interdum justo ex ac sapien. Vivamus turpis turpis, vestibulum sit amet sem ut, hendrerit facilisis lacus. Fusce porta sapien a elit dictum sollicitudin.
    		Quisque dictum diam ut diam interdum dictum. Morbi elementum porta dictum. Donec blandit ornare est, et fermentum sem vulputate quis. Donec vestibulum est ante. Nunc consequat neque eget suscipit interdum. Cras quis semper velit. Etiam ultricies finibus nibh, id laoreet urna posuere eu. Ut cursus neque elit, ut consectetur libero tempor ac. Nunc vitae volutpat nisl. Sed tincidunt, nunc ut aliquet euismod, sapien enim pharetra erat, vel semper felis diam quis leo. Etiam non scelerisque nibh, nec pulvinar ante. Integer commodo magna id quam gravida ultrices. Aenean cursus facilisis ipsum ut tempus. Aenean id porttitor tortor, et elementum turpis. Duis sed aliquet nisi.";
    	$message1->save();

    	$message2 = new Message;
    	$message2->email = 'm@correo.com';
    	$message2->name = "Antú";
    	$message2->surname = "Villegas";
    	$message2->subject = "Consulta cumpleaños";
    	$message2->body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas magna arcu, scelerisque ut ex iaculis, luctus rhoncus velit. Etiam gravida dictum elit, vitae fringilla velit dictum et. Mauris consequat egestas pretium. Mauris non mi sit amet ligula venenatis rhoncus nec vitae purus. Nullam lobortis, odio at elementum mollis, mi elit auctor augue, vitae interdum justo ex ac sapien. Vivamus turpis turpis, vestibulum sit amet sem ut, hendrerit facilisis lacus. Fusce porta sapien a elit dictum sollicitudin.
    		Quisque dictum diam ut diam interdum dictum. Morbi elementum porta dictum. Donec blandit ornare est, et fermentum sem vulputate quis. Donec vestibulum est ante. Nunc consequat neque eget suscipit interdum. Cras quis semper velit. Etiam ultricies finibus nibh, id laoreet urna posuere eu. Ut cursus neque elit, ut consectetur libero tempor ac. Nunc vitae volutpat nisl. Sed tincidunt, nunc ut aliquet euismod, sapien enim pharetra erat, vel semper felis diam quis leo. Etiam non scelerisque nibh, nec pulvinar ante. Integer commodo magna id quam gravida ultrices. Aenean cursus facilisis ipsum ut tempus. Aenean id porttitor tortor, et elementum turpis. Duis sed aliquet nisi.";
    	$message2->save();

    	$message3 = new Message;
    	$message3->email = 'z@correo.com';
    	$message3->name = "Antú";
    	$message3->surname = "Villegas";
    	$message3->subject = "Consulta despedida";
    	$message3->body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas magna arcu, scelerisque ut ex iaculis, luctus rhoncus velit. Etiam gravida dictum elit, vitae fringilla velit dictum et. Mauris consequat egestas pretium. Mauris non mi sit amet ligula venenatis rhoncus nec vitae purus. Nullam lobortis, odio at elementum mollis, mi elit auctor augue, vitae interdum justo ex ac sapien. Vivamus turpis turpis, vestibulum sit amet sem ut, hendrerit facilisis lacus. Fusce porta sapien a elit dictum sollicitudin.
    		Quisque dictum diam ut diam interdum dictum. Morbi elementum porta dictum. Donec blandit ornare est, et fermentum sem vulputate quis. Donec vestibulum est ante. Nunc consequat neque eget suscipit interdum. Cras quis semper velit. Etiam ultricies finibus nibh, id laoreet urna posuere eu. Ut cursus neque elit, ut consectetur libero tempor ac. Nunc vitae volutpat nisl. Sed tincidunt, nunc ut aliquet euismod, sapien enim pharetra erat, vel semper felis diam quis leo. Etiam non scelerisque nibh, nec pulvinar ante. Integer commodo magna id quam gravida ultrices. Aenean cursus facilisis ipsum ut tempus. Aenean id porttitor tortor, et elementum turpis. Duis sed aliquet nisi.";
    	$message3->save();
    }
}
