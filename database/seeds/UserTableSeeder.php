<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Antú",
            'surname' => "Villegas",
            'email' => 'antu.villegas96@gmail.com',
            'password' => bcrypt('1001')
        ]);
    }
}
