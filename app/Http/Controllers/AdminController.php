<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class AdminController extends Controller
{
	public function admin() {
		$user = Auth::user();
    	return view('admin.home', ['user' => $user]);
	}

	public function blocks($id){
		
		request()->validate([
			'icon' => 'required|string',
            'title' => 'required|string',
            'description' => 'required'
        ]);
        
		$block = \App\Block::find($id);

		$block->icon = request()->icon;
		$block->color = request()->color;
        $block->title = request()->title;
        $block->description = request()->description;
		
        $block->save();

        return redirect('/bloques')->with('success', "Bloque modificado");
	}

	public function cover(){

        request()->validate([
            'title' => 'required|string',
            'description' => 'required'
        ]);

        $cover = \App\Cover::get()->first();


        $cover->title = request()->title;
        $cover->description = request()->description;

        $cover->save();

        return redirect('/portada')->with('success','Portada modificada');

	}

	public function coverimg(){

        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);


        $imageName = time().'.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('assets/covers'), $imageName);

        $cover = \App\Cover::get()->first();

        $cover->url = '/assets/covers/' . $imageName;

        $cover->save();

        return redirect('/portada')->with('success','Portada modificada');;

	}

	public function update(){
		request()->validate([
	        'name' => 'required|string',
	        'surname' => 'required|string',
	        'email' => 'required|email',
	        'password' => 'required|confirmed',
	    ]);

	    $user = User::find(Auth::id());

	    $user->name = request()->name;
	    $user->surname = request()->surname;
	    $user->email = request()->email;
	    $user->password = bcrypt(request()->password);

	    $user->save();

	    Auth::logout();

	    return redirect('/login');
	}
}
