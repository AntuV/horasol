<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Message;

class AnswerController extends Controller
{
    public function create($email){
    	$messages = Message::latest()->where('email', $email)->get();
    	$messages = $messages->toArray();
    	return view('admin.answer', compact('email', 'messages'));
    }

    public function store(Request $request){
    	$request->validate([
    		'email' => 'required|email',
    		'subject' => 'required|string',
    		'body' => 'required|min:10'
    	]);
    	$msgdata = Message::where('email', $request->email)->first();
    	$message = new Message;

    	$message->name = $msgdata->name;
        $message->surname = $msgdata->surname;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->body = $request->body;
        $message->answer = true;
        $message->read = true;

        $message->save();

		/*Mail::to('antu.villegas96@gmail.com')->send(new MensajeRecibido($message));

        Mail::to($message->email)->send(new MensajeEnviado($message));*/

        return redirect('/mensajes')->with('success','Su respuesta ha sido enviada');
    }
}
