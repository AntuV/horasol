<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Block;
use App\Cover;

class HomeController extends Controller
{
    public function home() {
    	$blocks = Block::limit(3)->get();
    	$cover = Cover::get()->first();
        return view('home', ['blocks' => $blocks], ['cover' => $cover]);
    }
}
