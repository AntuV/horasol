<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Message;
use \App\Mail\MensajeRecibido;
use \App\Mail\MensajeEnviado;
use \Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{

    public function newMessages()
    {
        $this->middleware('auth');
        $messages = Message::latest()->where('read', '=', false)->get()->groupBy('email');
        return view('admin.messages', compact('messages'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        $messages = Message::latest()->get()->groupBy('email');
        return view('admin.messages', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $email)
    {
        $this->middleware('auth');

        $request->validate([
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|email',
            'subject' => 'required|max:60',
            'body' => 'required'
        ]);

        $message = new Message;

        $message->name = $request->name;
        $message->surname = $request->surname;
        $message->email = $email;
        $message->subject = $request->subject;
        $message->body = $request->body;

        $message->save();

        /*Mail::to('antu.villegas96@gmail.com')->send(new MensajeRecibido($message));

        Mail::to($message->email)->send(new MensajeEnviado($message));*/

        return redirect('/')->with('success','Su mensaje ha sido enviado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->middleware('auth');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->middleware('auth');
    }

    public function read(Request $request){
        $request->validate([
            'mail' => 'required|email'
        ]);
        $messages = Message::where('email', $request->mail)->get();
        foreach ($messages as $message) {
            $message->read = true;
            $message->save();
        }
        return json_encode(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->middleware('auth');
    }
}
