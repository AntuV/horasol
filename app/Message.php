<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'email', 'name', 'surname', 'subject', 'body', 'read', 'answer'
    ];

    public static function newcount(){
        $messages = Message::where('read', '=', false)->get();
        return count($messages);
    }
}
