<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $fillable = [
        'url', 'title', 'description'
    ];

    protected $table = 'cover';
}
