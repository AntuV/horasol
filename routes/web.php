<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home')->name('home');

Route::get('/contacto', 'MessageController@create')->name('contact');

Route::get('/servicios', function(){
	return view('services');
})->name('services');

Route::get('/admin', 'AdminController@admin')->middleware('auth')->name('admin');

Route::get('/portada', function(){
	$cover = \App\Cover::get()->first();
	return view('admin.cover', compact('cover'));
})->middleware('auth')->name('cover');

Route::get('/bloques', function(){
	$blocks = \App\Block::limit(3)->get();
	return view('admin.blocks', compact('blocks'));
})->middleware('auth')->name('blocks');

Route::patch('/portada', 'AdminController@cover')->middleware('auth')->name('coverupdate');

Route::patch('/portadaimg', 'AdminController@coverimg')->middleware('auth')->name('coverupdateimg');

Route::patch('/bloques/{id}', 'AdminController@blocks')->middleware('auth');

Route::get('/nuevos', 'MessageController@newMessages')->middleware('auth')->name('newMessages');

Route::post('/leido', 'MessageController@read')->middleware('auth');

Route::resource('message', 'MessageController');

Route::get('/mensajes', 'MessageController@index')->middleware('auth')->name('messages');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::patch('/user', 'AdminController@update')->middleware('auth')->name('user');

Auth::routes();

Route::get('/responder/{email}', 'AnswerController@create')->middleware('auth');
Route::post('/responder', 'AnswerController@store')->middleware('auth');