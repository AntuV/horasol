@if (count($blocks) >= 1)
    <div class="section">
        <div class="row">
            @foreach ($blocks as $block)
                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center {{ $block->color }}-text"><i class="material-icons">{{ $block->icon }}</i></h2>
                        <h5 class="center">{{ $block->title }}</h5>
                        <p class="light center">{{ $block->description }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif