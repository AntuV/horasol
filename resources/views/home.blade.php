@extends('layouts.app')

@section('title', 'Horasol')

@section('content')

  	@include ('parallax')

  	<div class="container">
  		@include ('blocks')
  	</div>

@endsection