@extends('admin.layouts.app')

@section('title', 'Horasol | Mensajes')

@section('content')
  	<main style="background-repeat: no-repeat; background-attachment: fixed;background-image: url('assets/contact.jpg'); min-height: 100vh; height: 100%">
    	<div class="container">
    		<div class="section">
		      	<div class="row">
		        	<div class="col s12">
		        		@if (count($messages) > 0)
			          		<ul class="collapsible" data-collapsible="accordion">
			          			@php
			          				function cmp($m1, $m2) {
			          					$a = $m1[0]['email'];
			          					$b = $m2[0]['email'];
									    return strcmp($a, $b);
									}
									$msgarray = $messages->toArray();
									uasort($msgarray, "cmp");
			          			@endphp
			          			
			          			@foreach ($msgarray as $msgusuario)

							    <li>
							      	<div onclick="read('{{ $msgusuario[0]['email'] }}')" class="collapsible-header"><i class="material-icons orange-text">message</i>{{ $msgusuario[0]['email'] }}</div>

							      	<div class="collapsible-body" style="background-color: #fafafa">
							      		<a href="{{ action('AnswerController@create', $msgusuario[0]['email']) }}" class="btn green right waves-effect waves-light">Responder</a><br>
							      		@foreach($msgusuario as $msg)
								      		@if ($loop->first)
								      			<h4 class="orange-text">{{ $msg['name'] . " " . $msg['surname'] }}</h4>
								      		@endif
						      				<div class="{{ $msg['answer'] ? "answer" : "" }}">
								      			<p class="{{ $msg['answer'] ? "left" : "right" }} light">{{ \Illuminate\Support\Carbon::createFromFormat('Y-m-d H:i:s', $msg['created_at'])->format('j/m/Y - h:i:s A') }}</p><br>
								      			@if (!$msg['answer'])
								      				<h5 class="amber-text">Asunto: {{ $msg['subject'] }}</h5>
								      			@else
								      				<h5 class="amber-text">Re:</h5>
								      			@endif
								      			<p>{{ $msg['body'] }}</p>
								      			@if (!$loop->last && $loop->count > 1)
								      				<hr>
								      			@endif
								      		</div>
							      		@endforeach
							      	</div>
							    </li>

							    @endforeach
							</ul>
						@else
							<div class="card">
								<div class="card-content">
									<h4 class="center light">
										No hay mensajes {{ Route::currentRouteName() == 'newMessages' ? "sin leer" : "" }}
									</h4>
								</div>
							</div>
						@endif
		        	</div>
		      	</div>
      		</div>
      	</div>
  	</main>
@endsection
@section ('scripts')
	<script type="text/javascript">
		function read(mail){
			$.ajax({
		        url: '{{ action('MessageController@read') }}',
		        type: 'POST',
		        dataType: 'json',
		        data: {mail: mail, _token: '{{ csrf_token() }}' },
		        success: function(data) {
		            if (data.success) {
		                
		            } else {
		                console.log(data);
		            }
		        }
		    });
		}
	</script>
	<script type="text/javascript">
	  $( document ).ready(function() {
	    $('.navcol').collapsible('open', 0);
	  });
	</script>
@endsection