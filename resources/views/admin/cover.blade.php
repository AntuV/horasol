@extends('admin.layouts.app')

@section('title', 'Horasol | Portada')

@section('content')
  	<main>
  		@include ('parallax')
    	<div class="container">
    		<div class="section">
    			<div class="row">
		        	<div class="col s12 m6">
		          		<div class="card center">
		            		<div class="card-content">
		            			<i class="material-icons orange-text large">text_fields</i>
		              			<span class="card-title">Datos de portada</span>
		              			<br>
		              			@if (count($errors))
		              				<p style="color:red">
			              				@foreach($errors->all() as $error)
			                            	<strong>{{ $error }}</strong><br>
			                            @endforeach
		                            </p>
		                        @endif
		              			<form method="POST" action="{{ route('coverupdate') }}">
		              				{{ csrf_field() }}
		              				<input name="_method" type="hidden" value="PATCH">
		              				<div class='input-field col s12'>
			              				<input type="text" id="title" name="title" value="{{ $cover->title }}">
			              				<label for="title">Título</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<textarea name="description" id="description" class="materialize-textarea">{{ $cover->description }}</textarea>
			              				<label for="description">Descripción</label>
			              			</div>
			              			<br>
			              			<center>
			                            <div class='row'>
			                                <button type="submit" class='col s6 offset-s3 btn btn-large waves-effect waves-light orange darken-2'>Guardar</button>
			                            </div>
			                        </center>
			              		</form>
			              	</div>
			            </div>
			        </div>
		        	<div class="col s12 m6">
		          		<div class="card center">
		            		<div class="card-content">
		            			<i class="material-icons orange-text large">add_a_photo</i>
		              			<span class="card-title">Imagen de portada</span>
		              			<br>
		              			@if (count($errors))
		              				<p style="color:red">
			              				@foreach($errors->all() as $error)
			                            	<strong>{{ $error }}</strong><br>
			                            @endforeach
		                            </p>
		                        @endif
		              			<form method="POST" enctype="multipart/form-data" action="{{ route('coverupdateimg') }}">
		              				{{ csrf_field() }}
		              				<input name="_method" type="hidden" value="PATCH">
			              			<div class="file-field input-field">
								      	<div class="btn orange darken-2">
								        	<span>Imagen</span>
								        	<input name="image" id="image" type="file">
								      	</div>
								      	<div class="file-path-wrapper">
								        	<input class="file-path validate" type="text">
								      	</div>
								    </div>
								    <br>
			              			<center>
			                            <div class='row'>
			                                <button type="submit" class='col s6 offset-s3 btn btn-large waves-effect waves-light orange darken-2'>Guardar</button>
			                            </div>
			                        </center>
		              			</form>
		            		</div>
		         		</div>
		      		</div>
      			</div>
      		</div>
      	</div>
  	</main>
@endsection

@section('scripts')
	<script type="text/javascript">
	  $( document ).ready(function() {
	    $('.navcol').collapsible('open', 1);
	  });
	</script>
@endsection