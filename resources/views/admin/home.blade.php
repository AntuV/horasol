@extends('admin.layouts.app')

@section('title', 'Horasol | Administrador')

@section('content')
  	<main style="background-repeat: no-repeat; background-attachment: fixed; background-image: url('/assets/contact.jpg')">
    	<div class="container">
    		<div class="section">
		      	<div class="row">
		        	<div class="col s12 m8 offset-m2">
		          		<div class="card center">
		            		<div class="card-content">
		            			<i class="material-icons orange-text large">person</i>
		              			<span class="card-title">Usuario</span>
		              			@if (count($errors))
		              				<p style="color:red">
			              				@foreach($errors->all() as $error)
			                            	<strong>{{ $error }}</strong><br>
			                            @endforeach
		                            </p>
		                        @endif
		              			<form method="POST" action="{{ route('user') }}">
		              				{{ csrf_field() }}
		              				<input name="_method" type="hidden" value="PATCH">
		              				<div class='input-field col s12'>
			              				<input type="text" id="name" name="name" value="{{ $user->name }}">
			              				<label for="name">Nombre</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<input type="text" id="surname" name="surname" value="{{ $user->surname }}">
			              				<label for="surname">Apellido</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<input type="text" id="email" name="email" value="{{ $user->email }}">
			              				<label for="email">Email</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<input type="password" id="password" name="password">
			              				<label for="password">Contraseña</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<input type="password" id="password_confirmation" name="password_confirmation">
			              				<label for="password_confirmation">Confirmar contraseña</label>
			              			</div>
			              			<center>
			                            <div class='row'>
			                                <button type="submit" class='col s6 offset-s3 btn btn-large waves-effect waves-light orange darken-2'>Guardar</button>
			                            </div>
			                        </center>
		              			</form>
		            		</div>
		         		</div>
		        	</div>
		      	</div>
      		</div>
      	</div>
  	</main>
@endsection