@extends('admin.layouts.app')

@section('title', 'Horasol | Mensajes')

@section('content')
  	<main style="background-repeat: no-repeat; background-attachment: fixed;background-image: url('/assets/contact.jpg'); min-height: 100vh; height: 100%">
    	<div class="container">
    		<div class="section">
		      	<div class="row">
		        	<div class="col s12">
		          		<ul class="collapsible" data-collapsible="accordion">
						    <li>
						      	<div class="collapsible-header"><i class="material-icons orange-text">message</i>{{ $email }}</div>

						      	<div class="collapsible-body" style="background-color: #fafafa">
						      		<h4 class="orange-text">{{ $messages[0]['name'] . " " . $messages[0]['surname'] }}</h4>
						      		@foreach($messages as $msg)
						      			<p class="right light">{{ \Illuminate\Support\Carbon::createFromFormat('Y-m-d H:i:s', $msg['created_at'])->format('j/m/Y - h:i:s A') }}</p><br>
						      			<h5>Asunto: {{ $msg['subject'] }}</h5>
						      			<p>{{ $msg['body'] }}</p>
						      			@if (!$loop->last && $loop->count > 1)
						      				<hr>
						      			@endif
						      		@endforeach
						      	</div>
						    </li>
						</ul>
		        	</div>
		      	</div>

		      	<div class="row">
		      		<div class="col s12">
		      			<div class="card">
		      				<div class="card-content">
		              			@if (count($errors))
		              				<p style="color:red">
			              				@foreach($errors->all() as $error)
			                            	<strong>{{ $error }}</strong><br>
			                            @endforeach
		                            </p>
		                        @endif
		      					<form method="POST" action="{{ action('AnswerController@store') }}">
		              				{{ csrf_field() }}
		              				<input type="hidden" name="email" value="{{ $email }}">
		              				<div class='input-field col s12'>
			              				<input type="text" id="subject" name="subject" value="Re: {{ \App\Message::latest()->where('email', $email)->first()->subject }}">
			              				<label for="subject">Asunto</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<textarea minlength="10" name="body" id="body" class="validate materialize-textarea"></textarea>
			              				<label for="body">Mensaje</label>
			              			</div>
			              			<br>
			              			<center>
			                            <div class='row'>
			                                <button type="submit" class='col s6 offset-s3 btn btn-large waves-effect waves-light orange darken-2'>Responder</button>
			                            </div>
			                        </center>
			              		</form>
		      				</div>
		      			</div>
		      		</div>
		      	</div>
      		</div>
      	</div>
  	</main>
@endsection