<header>
	<nav class="hide-on-large-only white">
		<div class="container">
			<a href="#" data-activates="nav-mobile" class="button-collapse top-nav full"><i class="material-icons black-text">menu</i></a>
		</div>
	</nav>
	<ul id="nav-mobile" class="side-nav fixed">
		<li>
			<div class="user-view">
				<div class="background">
					<img src="{{ asset('assets/background2.jpg') }}">
				</div>
				<img class="circle" src="{{ asset('assets/user.png') }}">
				<span class="white-text name">{{ $user->name . ' ' . $user->surname }}</span>
				<span class="white-text email">{{ $user->email }}</span>
			</div>
		</li>
		<li class="bold"><a href="{{ route('home') }}" class="waves-effect waves-orange">Página principal</a></li>
		<li class="bold {{ Route::currentRouteName() == 'admin' ? "active" : "" }}"><a href="{{ route('admin') }}" class="waves-effect waves-orange">Cuenta</a></li>
		<li class="no-padding">
			<ul class="collapsible navcol collapsible-accordion">
				<li class="bold">
					<a class="collapsible-header waves-effect waves-orange">Mensajes</a></div>
					<div class="collapsible-body">
						<ul>
							<li class="{{ Route::currentRouteName() == 'newMessages' ? "active" : "" }}">
								@if (\App\Message::newcount() > 0)
								<span style="cursor:pointer" onclick="javascript:window.location.assign('/nuevos');" class="new orange badge">{{ \App\Message::newcount() }}</span>
								@endif
								<a href="/nuevos">Nuevos</a>
							</li>
							<li class="{{ Route::currentRouteName() == 'messages' ? "active" : "" }}"><a href="/mensajes">Todos</a></li>
						</ul>
					</div>
				</li>
				<li class="bold"><a class="collapsible-header waves-effect waves-orange">Diseño</a>
					<div class="collapsible-body">
						<ul>
							<li class="{{ Route::currentRouteName() == 'cover' ? "active" : "" }}"><a href="/portada">Portada</a></li>
							<li class="{{ Route::currentRouteName() == 'blocks' ? "active" : "" }}"><a href="/bloques">Bloques</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</li>
		<li class="bold"><a href="{{ route('logout') }}" class="waves-effect waves-orange">Cerrar sesión</a></li>
	</ul>
</header>