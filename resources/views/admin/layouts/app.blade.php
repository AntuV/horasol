<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/css/admin.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  </head>
  <body>
    @include('admin.layouts.partials.navbar')

    @yield('content')

    @include('admin.layouts.partials.footer')
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/js/materialize.min.js"></script>
    <script src="/js/init.js"></script>
    @if(session()->has('success'))
        <script type="text/javascript">
          (function($){
            $(function(){
              var $toastContent = $('<span>{{ session()->get('success') }}</span>').add($('<button onclick="removeToast()" class="btn-flat toast-action">Cerrar</button>'));
                Materialize.toast($toastContent, 10000);
            })
          })(jQuery);
          function removeToast(){
            var toastElement = $('.toast').first()[0];
            var toastInstance = toastElement.M_Toast;
            toastInstance.remove();
          }
        </script>
    @endif
    @yield('scripts')
  </body>
</html>
