@extends('admin.layouts.app')

@section('title', 'Horasol | Bloques')

@section('content')
  	<main>
    	@include ('blocks')
    	<hr>
    	<div class="section">
    		<div class="row">
    			<div class="col s12 center">
			    	<a class="btn orange darken-2 waves-effect waves-light" target="_blank" href="http://materializecss.com/icons.html">
			    		Lista de íconos
			    	</a>
    			</div>
    		</div>
    	</div>
    	<div class="section">
	    	<div class="row">
	    		@foreach ($blocks as $block)
			    	<div class="col s12 m4">
		          		<div class="card center">
		            		<div class="card-content">
		            			<i class="material-icons orange-text large">text_fields</i>
		              			<span class="card-title">Bloque #{{ $block->id }}</span>
		              			<br>
		              			@if (count($errors))
		              				<p style="color:red">
			              				@foreach($errors->all() as $error)
			                            	<strong>{{ $error }}</strong><br>
			                            @endforeach
		                            </p>
		                        @endif
		              			<form method="POST" action="{{ action('AdminController@blocks', $block->id)}}">
		              				{{ csrf_field() }}
		              				<input name="_method" type="hidden" value="PATCH">
		              				<div class='input-field col s12'>
			              				<input type="text" id="icon" name="icon" value="{{ $block->icon }}">
			              				<label for="icon">Ícono</label></a>
			              			</div>
			              			<div class='input-field col s12'>
			              				<select id="color" name="color">
			              					@foreach (\App\Color::all()->sortBy('name_spanish') as $color)
									      		<option {{ $color->name_english == $block->color ? "selected" : ""}} value="{{ $color->name_english }}">{{ $color->name_spanish }}</option>
									      	@endforeach
									    </select>
									    <label>Color</label>
			              			</div>
		              				<div class='input-field col s12'>
			              				<input type="text" id="title" name="title" value="{{ $block->title }}">
			              				<label for="title">Título</label>
			              			</div>
			              			<div class='input-field col s12'>
			              				<textarea name="description" id="description" class="materialize-textarea">{{ $block->description }}</textarea>
			              				<label for="description">Descripción</label>
			              			</div>
			              			<br>
			              			<center>
			                            <div class='row'>
			                                <button type="submit" class='col s6 offset-s3 btn btn-large waves-effect waves-light orange darken-2'>Guardar</button>
			                            </div>
			                        </center>
			              		</form>
			              	</div>
			            </div>
			        </div>
		        @endforeach
	        </div>
        </div>
  	</main>
@endsection

@section('scripts')
	<script type="text/javascript">
	  $( document ).ready(function() {
	    $('.navcol').collapsible('open', 1);
	  });
	</script>
@endsection