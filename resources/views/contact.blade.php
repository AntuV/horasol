@extends('layouts.app')

@section('title', 'Horasol | Contacto')

@section('content')
<main style="background-image: url('assets/contact.jpg')">
	<div class="section"></div>
	<div class="container">
		<div class="section">
			<div class="row">
				<div class="col s12 m10 offset-m1">
					<div class="card">
						<div class="card-content">
							<div class="container">
								<br>
								<h3>Dejanos tu consulta</h3>
								<br>
								<form method="POST" action="{{ action('MessageController@store') }}">
									{{ csrf_field() }}
									<div class="row">
										<div class='input-field col s12 m6'>
				              				<input maxlength="60" type="text" id="name" name="name" value="{{ old('name') }}">
				              				<label for="name">Nombre/s</label>
				              			</div>
				              			<div class='input-field col s12 m6'>
				              				<input maxlength="60" type="text" id="surname" name="surname" value="{{ old('surname') }}">
				              				<label for="surname">Apellido/s</label>
				              			</div>
			              			</div>
			              			<div class="row">
										<div class='input-field col s12'>
				              				<input type="email" id="email" name="email" value="{{ old('name') }}">
				              				<label for="email">Correo electrónico</label>
				              			</div>
			              			</div>
			              			<div class="row">
				              			<div class='input-field col s12'>
				              				<input maxlength="60" type="text" id="subject" name="subject" value="{{ old('subject') }}">
				              				<label for="subject">Asunto</label>
				              			</div>
			              			</div>
			              			<div class="row">
				              			<div class='input-field col s12'>
				              				<textarea rows="4" name="body" id="body" class="materialize-textarea">{{ old('body') }}</textarea>
				              				<label for="body">Mensaje</label>
				              			</div>
			              			</div>
		                            <div class='row'>
		                                <button type="submit" class='right btn waves-effect waves-light light-green accent-4'>Enviar</button>
		                            </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection