@component('mail::message')
# Nuevo mensaje recibido

**De**: {{ $message->email }}

**Asunto**: {{ $message->subject }}

@component('mail::button', ['url' => action('MessageController@show', $message->id), 'color' => 'green'])
Ver mensaje
@endcomponent

@endcomponent
