@component('mail::message')
# Gracias por enviar su mensaje

**Asunto**: {{ $message->subject }}

**Mensaje**: {{ $message->body }}

@component('mail::button', ['url' => action('HomeController@home'), 'color' => 'green'])
Visitar web
@endcomponent

@endcomponent
