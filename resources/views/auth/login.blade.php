<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>@yield('title')</title>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        body {
            background: #fafafa;
        }

        .input-field input[type=date]:focus+label,
        .input-field input[type=text]:focus+label,
        .input-field input[type=email]:focus+label,
        .input-field input[type=password]:focus+label {
            color: #ff9800 !important;
        }

        .input-field input[type=date]:focus,
        .input-field input[type=text]:focus,
        .input-field input[type=email]:focus,
        .input-field input[type=password]:focus {
            border-bottom: 2px solid #ff9800 !important;
            box-shadow: none !important;
        }

        @media only screen and (max-width: 600px) {
            #logincontainer {
                width: 100%;
                margin-bottom: 0px;
                border: none;
                height: 100vh;
                padding: 15px 15px 0px 15px !important;
            }

            .container {
                width: 100%;
            }
        }

        [type="checkbox"].filled-in:checked + label::after {
            border: 2px solid #f57c00 !important;
            background-color: #f57c00 !important;
        }
  </style>
</head>
<body>
    <main style="padding-left:0;">
        <center>
            <div class="section hide-on-small-only"></div>
            <div class="container">
                <div id="logincontainer" class="z-depth-1 white row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
                    <div class="center">
                        <img src="assets/horasol.png" class="responsive-img" style="width: 30%;">
                    </div>
                    <form id="loginForm" class="col s12" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        @if (count($errors))
                            <div class='row'>
                                <div class='col s12'>
                                    <p id="error" class="red-text center">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}
                                            @unless ($loop->last)
                                                <br>
                                            @endunless
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        @endif

                        <div class='row'>
                            <div class='input-field col s12'>
                                <input type='text' name='email' id='email' value="{{ old('email') }}" />
                                <label for='email'>Ingresa tu correo</label>
                            </div>
                        </div>

                        <div class='row'>
                            <div class='input-field col s12'>
                                <input type='password' name='password' id='password' />
                                <label for='password'>Ingresa tu contraseña</label>
                            </div>
                            <label style='float: right;'>
                                <a class='green-text' href="{{ route('password.request') }}"><b>¿Olvidaste tu contraseña?</b></a>
                            </label>
                        </div>

                        <div class='row'>
                            <div class="col">
                                <input type="checkbox" class="filled-in" id="filled-in-box" name="remember" {{ old('remember') ? 'checked' : '' }}" />
                                <label for="filled-in-box">Recordarme</label>
                            </div>
                        </div>

                        <br />
                        <center>
                            <div class='row'>
                                <button type="submit" id="login" class='col s12 btn btn-large waves-effect waves-light orange darken-2'>Iniciar sesión</button>
                            </div>
                        </center>
                    </form>
                </div>
            </div>
        </center>
    </main>
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/init.js"></script>
</body>
</html>
