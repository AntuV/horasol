<footer class="page-footer light-green accent-4">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="black-text">Sobre nosotros</h5>
        <p class="grey-text text-darken-4">Somos bla bla bla</p>


      </div>
      <div class="col l3 offset-l3 s12">
        <h5 class="black-text">Mapa de sitio</h5>
        <ul>
          <li><a class="black-text" href="{{ route('home') }}">Inicio</a></li>
          <li><a class="black-text" href="{{ route('services') }}">Servicios</a></li>
          <li><a class="black-text" href="{{ route('contact') }}">Contacto</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright grey-text text-darken-3">
    <div class="container">
    Desarrollado por <a class="black-text" target="_blank" href="http://www.facebook.com/AntuV">Leandro Antú Villegas</a>
    </div>
  </div>
</footer>