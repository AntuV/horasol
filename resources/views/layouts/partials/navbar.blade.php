<nav class="grey lighten-5" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="{{ route('home') }}" class="brand-logo">
      <img src="assets/horasol.png" height="64">
    </a>
    <ul class="right hide-on-med-and-down">
      <li><a href="{{ route('home') }}">Inicio</a></li>
      <li><a href="{{ route('services') }}">Servicios</a></li>
      <li><a href="{{ route('contact') }}">Contacto</a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav">
      <li><a href="{{ route('home') }}">Inicio</a></li>
      <li><a href="{{ route('services') }}">Servicios</a></li>
      <li><a href="{{ route('contact') }}">Contacto</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
  </div>
</nav>