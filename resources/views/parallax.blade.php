@if (count($cover) == 1)
  	<div id="index-banner" class="parallax-container">
    	<div class="section no-pad-bot">
      		<div class="container">
        		<br><br>
    			<h1 class="header center amber-text">{{ $cover->title }}</h1>
        		<div class="row center">
          			<h5 class="header col s12 light white-text">{{ $cover->description }}</h5>
        		</div>
        		<div class="row center">
          			<a href="#" id="download-button" class="hoverable btn-large waves-effect waves-light amber darken-2">Contactanos</a>
        		</div>
        		<br><br>

  			</div>
		</div>
    	<div class="parallax"><img src="{{ $cover->url }}" alt="cover"></div>
  	</div>
@endif